/*                                                                                  
  Greg Langer/600.120/June 14 2013/Assignment 4/646.265.1591/glanger1/greglanger120@gmail.com

This part took me about an hour. Most of that was spent trying to figure out how to do the random numbers. Then I found the examples from class. I needed help realizing that when I open the wrong database (tengrand.db instead of tenGrand.db) I should expect some large failures. I learned (although I should have already known) how to do random number, which should be helpful in future endeavors. The best part of the assignment was that it didn't take an absurdly long time because by the time I got here, I had already figured out most of the errors in sdbm.c. If I were to change this assignment, I'd make students print out the database when full.

Also there's a 1.245e-6% chance that it'll only print 9999 entries, so if that happens, sorry. Running it again will probably fix that.


*/    

#include "sdbm.h"
#include "player.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <stdio.h>

double randDouble(void)
{
  return (double)rand()/RAND_MAX;
}

int main(void)
{
  int sizeOfAngryMob=10000;
  srand(time(NULL));

  Player **angryMob=(Player**)malloc(sizeof(Player*)*sizeOfAngryMob);
  
  
  sdbm_create("tenGrand.db");
  sdbm_open("tenGrand.db");
  char temp[8];
    for(int i=0;i<sizeOfAngryMob;i++)
      {
	angryMob[i]=(Player*)malloc(sizeof(Player));

	temp[0]=26*randDouble()+'a';
	temp[1]=26*randDouble()+'a';
	temp[2]=26*randDouble()+'a';
	temp[3]=26*randDouble()+'a';
	temp[4]=26*randDouble()+'a';
	temp[5]=26*randDouble()+'a';
	temp[6]=26*randDouble()+'a';
	temp[7]='\0';

	strcpy(angryMob[i]->name,temp);	
	angryMob[i]->playerID=0;
	angryMob[i]->health=100*randDouble();
	angryMob[i]->mana=100*randDouble();
	angryMob[i]->experience=100000*randDouble();
	angryMob[i]->x=100*randDouble();
	angryMob[i]->y=100*randDouble();

	sdbm_insert(angryMob[i]->name,angryMob[i]);
      }
    sdbm_close();


    sdbm_open("tenGrand.db");

    for(int j=0;j<sizeOfAngryMob;j++)
      {
	sdbm_remove(angryMob[j]->name);
	free(angryMob[j]);
      }

    free(angryMob);
    sdbm_close();

  return 0;
}
