/*                                                                              
  Greg Langer/600.120/June 14 2013/Assignment 4/646.265.1591/glanger1/greglange\
r120@gmail.com                                                                  
  
The bulk of my comments are in the header for coverage.c for this assignment. I worked on them simultaneously to build and test so I could tell right away if I was building garbage. 

One of the hardest things about this one was the sdbm_get function because for some reason I couldn't get strcpy to work. I ended up using a character by character transfer.

Overall (just this file and coverage.c) this took my a solid 18ish hours. 
                 
*/


#include "sdbm.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "player.h"

/**
 * Minimum and maximum length of key and name strings,
 * including '\0' terminator.
 */
#define MIN_KEY_LENGTH 3
#define MAX_KEY_LENGTH 16

int sizeDB=8; //start size, can be changed as necessary (originally 10, but changed to 8 because it's a power of 2 and therefore safer with realloc when it halves the size of the RAM DB
int maxDBsize=8;
int numPlayers=0; //keeps track of how many players currently in RAM db
int pIndex=0; //way of indexing playerIDs
Player **players;
FILE * db;
char * globalName;

int error=0;
enum{
  DB_EXISTS=1,
  DB_NOT_EXIST,//2
  NAME_MISMATCH,//3
  PLAYER_IN_DB,//4
  PLAYER_NAME_TAKEN,//5
  PLAYER_NOT_FOUND,//6
  DB_READ_ERROR,//7
  NULL_DB,//8
  SYNC_FAILED,//9
  FILE_ALREADY_OPEN,//10
  NULL_VALUE,//11
  NAME_OUT_OF_BOUNDS//12
};

/**
 * Create new database with given name. You still have
 * to sdbm_open() the database to access it. Return true
 * on success, false on failure.
 */
bool sdbm_create(const char *name)
{
  db = fopen(name,"r");
  if(db==NULL)
    {
      db=fopen(name,"w");
      fclose(db); //still need to use sdbm_open to access it, but now it exists
      db=NULL;
      return true;
    }
  else
    {
      fclose(db);
      db=NULL;
      error=DB_EXISTS;
      return false;
    }
}

/**
 * Open existing database with given name. Return true on
 * success, false on failure.
 */
bool sdbm_open(const char *name)
{
  if(db==NULL)
    {
      db = fopen(name,"r");
      
      //globalName=(char*)malloc(strlen(name)+1);
      //strcpy(globalName,name);
      
      if(db==NULL)
	{
	  error=DB_NOT_EXIST;
	  return false;
	}
      else
	{
	  //globalize the name of the open db so others (sync,close) can see it
	  globalName=(char*)malloc(strlen(name)+1);
	  strcpy(globalName,name);
	  
	  //dump all previous entries, if any
	  //if(numPlayers>0)
	  //  {
	  //    for(int i=0;i<sizeDB;i++)
	  //	{
	  //	  free(players[i]);
	  //	}
	  //  free(players);
	  //  }
	  numPlayers=0;
	  sizeDB=8;

	  players=(Player**)malloc(sizeDB*sizeof(Player*));
	  
	  //read all new entries into RAM
	  for(int i=0;i<sizeDB;i++)
	    {
	      players[i]=(Player*)malloc(sizeof(Player));
	    }

	  Player *temp=(Player*)malloc(sizeof(Player));	 
	  long diskDBsize;
	  int result;
	  
	  fseek(db,0,SEEK_END);
	  diskDBsize=ftell(db);
	  fseek(db,0,SEEK_SET);
	  // printf("file size: %d\n",(int)diskDBsize);
	  // printf("sizeof Player: %d\n",(int)sizeof(Player));

	  int numEls=(int)diskDBsize/sizeof(Player);
	  // printf("num elements %d\n",numEls);
	  if(numEls>=1)
	    {

	      // printf("load the db\n");

	      //int numEls=diskDBsize/sizeof(Player);
	      for(int i=0;i<numEls;i++)
		{
		  //result=fread(temp,sizeof(Player)/numEls,numEls,db);
		  result=fread(temp,sizeof(Player),1,db);
		  //		  printf("DATABASE READ - name: %s\n",temp->name);

		  if(result==1)
		    {
		      sdbm_insert(temp->name,temp);
		    }

//		    {
//		      printf("error!");
//		      error=DB_READ_ERROR;
//		      return false;
//		      } 
		}
	      //free(temp);
	      // printf("numplayers:%d\n",numPlayers);
	      //return true;

	    }
	  free(temp);
	  return true;
	}
    }
  else
    {
      error=FILE_ALREADY_OPEN;
      return false;
    }
}

/**
 * Synchronize all changes in database (if any) to disk.
 * Useful if implementation caches intermediate results
 * in memory instead of writing them to disk directly.
 * Return true on success, false on failure.
 */
bool sdbm_sync()
{
  if(db)
    {
      fclose(db); //close database (read only)
      db=fopen(globalName,"w");//open database to write

      //write all players to db, close db (write)
      for(int i=0;i<numPlayers;i++)
	{
	  fwrite(players[i],sizeof(Player),1,db);
	}
      fclose(db);

      db=NULL;
      //open the file again in read only mode (Should I import to RAM? Probably not.)
      db=fopen(globalName,"r");
     
      return true;
    }

  error=NULL_DB;
  return false;
}

/**
 * Close database, synchronizing changes (if any). Return
 * true on success, false on failure
 */
bool sdbm_close()
{
  if(sdbm_sync())
    {
      fclose(db);
      
      for(int i=0;i<sizeDB;i++)
	{
	    free(players[i]);
	}
      free(players);
      free(globalName);
      /*
      for(int i=0;i<numPlayers;i++)
	{
	printf("%s %d %d %d %f %f\n",players[i]->name,players[i]->playerID,players[i]->health,players[i]->experience,players[i]->x,players[i]->y);    
	}
      */


      db=NULL;
      numPlayers=0;
      return true;
    }
  else
    {
      error=SYNC_FAILED;
      return false;
    }
}

/**
 * Return error code for last failed database operation.
 */
int sdbm_error()
{
  int temp;
  temp=error;
  error=0;
  return temp;
}

/**
 * Is given key in database?
 */
bool sdbm_has(const char *key)
{
  bool temp=false;

  for(int i=0;i<numPlayers;i++)
    {
      if(strcmp(players[i]->name,key)==0)
	{
	  temp=true;
	}
    }
  return temp;
}

/**
 * Get value associated with given key in database.
 * Return true on success, false on failure.
 *
 * Precondition: sdbm_has(key)
 */

bool sdbm_get(const char *key, Player *value)
{
  if(sdbm_has(key))
    {
      for(int i=0;i<numPlayers;i++)
	{
	  if(strcmp(players[i]->name,key)==0)
	    {
	      //printf("i:%d key:%s name:%s\n",i,key,players[i]->name);
	    
	      //value=players[i];

	      char ch;
	      int j=0;
	      while((ch=(players[i]->name)[j]))
		{
		  (value->name)[j]=ch;
		  j++;
		}
	      (value->name)[j]='\0';

	      //I HAVE NO IDEA WHY STRCPY IS NOT WORKING HERE, SO I HAD TO DO IT CHARACTER BY CHARACTER WHICH IS ANNOYING, BUT HEY- IT WORKS.
		//strcpy(tempo,value->name);

	      players[i]->playerID=value->playerID;
	      players[i]->health=value->health;
	      players[i]->mana=value->mana;
	      players[i]->experience=value->experience;
	      players[i]->x=value->x;
	      players[i]->y=value->y;
	      
	    }
	}
    }
  else
    {
      error=PLAYER_NOT_FOUND;
      return false;
    }
  return true;
  /*  if(value==NULL)
    {
    error=NULL_VALUE;
    return false;
    }
  else
    return true;
*/
}

/**
 * Update value associated with given key in database
 * to given value. Return true on success, false on
 * failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_put(const char *key, const Player *value)
{
  if(sdbm_has(key))
    {
      if((!sdbm_has(value->name)&&strcmp(key,value->name)==0)||(sdbm_has(value->name)&&strcmp(key,value->name)!=0)) //(name is in db)XOR(player has name "key") 
	{
	  for(int i=0;i<numPlayers;i++)
	    {
	      if(strcmp(players[i]->name,key)==0)
		{

		  //  printf("PUT - changing: %s to %s\n",players[i]->name,value->name);
		 
		  strcpy(players[i]->name,value->name);
		  players[i]->health=value->health;
		  players[i]->mana=value->mana;
		  players[i]->experience=value->experience;
		  players[i]->x=value->x;
		  players[i]->y=value->y;

		  for(int i=0;i<numPlayers;i++)
		    printf("PUT - name: %s\n",players[i]->name);

		}
	    }
	}
      else
	{
	  error=PLAYER_NAME_TAKEN;
	  return false;
	}
    }
  else
    {
      error=PLAYER_NOT_FOUND;
      return false;
    }
  return true;
}

/**
 * Insert given key and value into database as a new
 * association. Return true on success, false on
 * failure.
 *
 * Precondition: !sdbm_has(key)
 */
bool sdbm_insert(const char *key, const Player *value)
{
  if((int)strlen(key) > MIN_KEY_LENGTH && (int)strlen(key) < MAX_KEY_LENGTH)
    {
      if(!sdbm_has(key))
	{
	  if(strcmp(value->name,key)==0)
	    {
	      if(numPlayers<=sizeDB-1)
		{
		  printf("%d\n",numPlayers);
		  strcpy(players[numPlayers]->name,value->name);
		  players[numPlayers]->playerID=pIndex;
		  players[numPlayers]->health=value->health;
		  players[numPlayers]->mana=value->mana;
		  players[numPlayers]->experience=value->experience;
		  players[numPlayers]->x=value->x;
		  players[numPlayers]->y=value->y;
		  
		}
	      else
		{
		  // printf("realloc, numPlay=%d\n",numPlayers);
		  int prevSize=sizeDB;
		  sizeDB*=2;
		  		  maxDBsize=sizeDB;
		  players=(Player**)realloc(players,sizeDB*sizeof(Player*));
		  
		  for(int p=prevSize;p<sizeDB;p++)
		    {
		      players[p]=(Player*)malloc(sizeof(Player));
		    }
		  
		  strcpy(players[numPlayers]->name,value->name);
		  players[numPlayers]->playerID=pIndex;
		  players[numPlayers]->health=value->health;
		  players[numPlayers]->mana=value->mana;
		  players[numPlayers]->experience=value->experience;
		  players[numPlayers]->x=value->x;
		  players[numPlayers]->y=value->y;
		  
		  printf("realloc, size players=%d, sizeDB=%d, numPlayers=%d\n",(int)sizeof(players),sizeDB,numPlayers);

		}

	      //	      printf("INSERT - name: %s\n",players[numPlayers]->name);

	      pIndex++;
	      numPlayers++;
	      /*
	      	      for(int i=0;i<numPlayers;i++)
                printf("INSERT - name: %s\n",players[i]->name);
	      */
	    }
	  else
	    {
	      error=NAME_MISMATCH;
	      return false;
	    }
	}
      else
	{
	  error=PLAYER_IN_DB;
	  return false;
	}
      
      // printf("%d\n",numPlayers-1);
      // printf("%s %d %d %d %f %f\n",players[numPlayers-1]->name,players[numPlayers-1]->playerID,players[numPlayers-1]->health,players[numPlayers-1]->experience,players[numPlayers-1]->x,players[numPlayers-1]->y);
      
      
      return true;
    }
  else
    {
      error=NAME_OUT_OF_BOUNDS;
      return false;
    }
}

/**
 * Remove given key and associated value from database.
 * Return true on success, false on failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_remove(const char *key)
{
  if(sdbm_has(key)) //check valid player
    {
      for(int i=0;i<numPlayers;i++) //scan player index
	{
	  if(strcmp(players[i]->name,key)==0) //find player
	    {
	      printf("REMOVE - name: %s\n",key);
	      for(int j=i;j<numPlayers-1;j++) //shift remaining players
		{
		  if((players[j]->name)!=NULL) //remaining valid player
		    {
		      strcpy(players[j]->name,players[j+1]->name);
		      players[j]->playerID=players[j+1]->playerID;
		      players[j]->health=players[j+1]->health;
		      players[j]->mana=players[j+1]->mana;
		      players[j]->experience=players[j+1]->experience;
		      players[j]->x=players[j+1]->x;
		      players[j]->y=players[j+1]->y;		      
		    }
		}
	      char nullname[1]={'\0'}; //strcpy didn't like me passing NULL directly
	      strcpy(players[numPlayers]->name,nullname); //sets the last (previously valid) element to null
	      
	      numPlayers--;
	      //printf("%d\n",numPlayers);
	    }
	}
      if(4*numPlayers<=sizeDB)
	{
	  for(int i=sizeDB/2;i<sizeDB;i++)
	    {
	      free(players[i]);
	    }
	  sizeDB/=2;
	  players=(Player**)realloc(players,sizeDB*sizeof(Player*));
	}


    }
  else
    {
      error=PLAYER_NOT_FOUND;
      return false;
    }
  return true;
}
