#ifndef _GAME_H
#define _GAME_H

#define FIREBALL_DAMAGE 30
#define ARROW_DAMAGE 40

int getPlayerIndex(const char * name);
bool gameOver();
bool isAlive(int);
void fireball(int, int);
void arrow(int, int);
char* getPlayerName(int);
void checkValidPlayer(int);
void printTurn();

#endif //_GAME_H
