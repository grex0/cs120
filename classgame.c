/* Improvements:
- ask user how many players
- restrict who shoots what, add more weapons
- group player data together into one unit
*/

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "game.h"
#include "player.h"

#include "sdbm.h"

//Global variables
Player * players = 0;
int NUM_PLAYERS;

int main(int argc, char *argv[])
{
	char name[21];
	int np;

	if (!sdbm_open("game.db"))
	{
		if (!sdbm_create("game.db"))
		{
			printf("Failed to create file\n");
			return 0;
		}

		sdbm_open("game.db");
	}

	/*
	sdbm_has("joanne");
	Player james;
	if (sdbm_has("james"))
		sdbm_get("james", &james);
	*/


	//Make sure two values are specified
	if (argc != 2) {
		printf("Usage: game numPlayers\n");
		return 0;
	}

	NUM_PLAYERS = atoi(argv[1]);
	np = NUM_PLAYERS;
	players = calloc(NUM_PLAYERS, sizeof(Player));

	printf("enter the players names\n");
	for (int i = 0; i < NUM_PLAYERS; i++) {
		scanf("%s", name);
		if (sdbm_has(name))
			sdbm_get(name, players + i);
		else
		{
			Player player = { "", 0, 100, 80, 10000, 10.0f, 0.0f };
			strcpy(player.name, name);
			sdbm_insert(name, &player);
		}
	}

	printf("Success!");
	return 0;

	int turn = -1;
	while (!gameOver()) {
		turn = (turn + 1) % NUM_PLAYERS;	// roll over to 0

		printTurn(turn);

		//Attack and target tokens longer than 9 characters
		//will not work 
		char attack[10], target[10];
		scanf("%s %s", attack, target);

		int playerIndex = getPlayerIndex(target);

		if (strcmp(attack, "fireball") == 0)
			fireball(turn, playerIndex);
		else if (strcmp(attack, "arrow") == 0)
			arrow(turn, playerIndex);
		else
			printf("Invalid spell!\n");

		if (!isAlive(playerIndex) && turn >= playerIndex)
			turn--;

	}

	printf("winner is %s!\n", players->name);	// shorthand for players[0].name

	for (int i = 0; i < NUM_PLAYERS; i++)
		free(players[i].name);
	free(players);

	return 0;
}

int getPlayerIndex(const char *name)
{
	for (int i = 0; i < NUM_PLAYERS; i++)
		if (strcmp(name, players[i].name) == 0)
			return i;

	//We haven't found the name!
	printf("Invalid target! Type a new player name:\n");

	char newName[10];
	scanf("%s", newName);

	return getPlayerIndex(newName);
}

bool gameOver()
{
	int count = 0;
	Player temp;

	for (int i = 0; i < NUM_PLAYERS; i++) {
		if (isAlive(i))
			count++;
		else {
			printf("%s is DEAD\n", players[i].name);
			temp = players[i];
			for (int j = i + 1; j < NUM_PLAYERS; j++)
				players[j - 1] = players[j];
			players[--NUM_PLAYERS] = temp;
			i--;	// backup a player
		}
	}

	return count == 1;
}

bool isAlive(int player)
{
	checkValidPlayer(player);
	return players[player].health > 0;
}

void printTurn(int player)
{
	for (int i = 0; i < NUM_PLAYERS; i++) {
		printf("%s now has %d health, ", players[i].name,
		       players[i].health);
	}
	printf("\n%s's turn\n", getPlayerName(player));
}

void fireball(int source, int target)
{
	checkValidPlayer(source);
	checkValidPlayer(target);

	//Decrease target's health
	players[target].health -= FIREBALL_DAMAGE;

	printf("%s cast fireball at %s, dealing %d damage\n",
	       getPlayerName(source), getPlayerName(target), FIREBALL_DAMAGE);
}

void arrow(int source, int target)
{
	checkValidPlayer(source);
	checkValidPlayer(target);

	//Decrease target's health
	players[target].health -= ARROW_DAMAGE;

	printf("%s shot an arrow at %s, dealing %d damage\n",
	       getPlayerName(source), getPlayerName(target), ARROW_DAMAGE);
}

/** Returns the player's name from the array of names base on his or her index.
*/
char *getPlayerName(int player)
{
	checkValidPlayer(player);
	return players[player].name;
}

/** Checks if the player (integer) is valid. Exits the program on failure.
*/
void checkValidPlayer(int player)
{
	assert(player >= 0);
	assert(player < NUM_PLAYERS);
}
