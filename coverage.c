/*
  Greg Langer/600.120/June 14 2013/Assignment 4/646.265.1591/glanger1/greglanger120@gmail.com

  This program was tricky, but didn't cause me as much grief as the last program. I wrote this one specifically alongside my sdbm.c file, so much of the reflection will probably be the same between the two. I wanted to write the test code as I implemented the sdbm.h file. The hardest part of this one was actually part of the sdbm file, but I dealt with it at the same time. Memory leaks. The source of my worst memory leak problem was when I was shrinking the array. I now know that I had to free that memory before I could change the size. I thought realloc would take care of it, but I had malloc'd space for the elements themselves, so I had to take care of that. Consequently, that was also the most important thing I learned from this assignment.

I needed help finding my memory leaks and just general small debugging issues. The best part of the assignment was the lightbulb moment when I realized the memory was getting lost at the realloc. If I was given the option to change something about this assignment, I honestly don't think I would. I thought it was a very fair way about going about teaching us databases and reallocation. Having said that though, I would have made it more clear that blockIO is pretty much mandatory for this type of project. I thankfully figured that out pretty quickly, but it seems like a large number of people I talked to didn't realize that's what they were supposed to do. Many of them were making text files.

Other than that though, this one went quite well. I spent the first day just planning it out, then the second day I wrote most of it, then the 3rd day I finished debugging and tweaking it.

Note: you get 100% coverage when the database doesn't exist initially. Took me a few minutes to figure out that one.
 */


#include "sdbm.h"
#include <stdbool.h>
#include "player.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
  sdbm_close(); //sync should fail b/c db=NULL

  //testing sdbm_create
  char dtb[]={"test.db"};
  
  if(sdbm_create(dtb))
    printf("success create\n");
  else
    printf("fail create %d\n",sdbm_error());
  
//test it again so that it fails, assuming it succeeded the first time
  if(sdbm_create(dtb))
    printf("success create\n");
  else
    printf("fail create %d\n",sdbm_error());
  
  //testing sdbm_open
  if(sdbm_open("meow"))
    printf("success open\n");
  else
    printf("fail open %d\n",sdbm_error());
 
 if(sdbm_open(dtb))
    printf("success open\n");
  else
    printf("fail open %d\n",sdbm_error());


 int nPlay=16;

 Player **people=(Player**)malloc(sizeof(Player*)*nPlay);
  
 Player p1={"aaaa",0,100,80,10000,10.0f,0.0f};
 Player p2={"aaab",0,100,80,10000,10.0f,0.0f};
 Player p3={"aaac",0,100,80,10000,10.0f,0.0f};
 Player p4={"aaad",0,100,80,10000,10.0f,0.0f};
 Player p5={"aaae",0,100,80,10000,10.0f,0.0f};
 Player p6={"aaaf",0,100,80,10000,10.0f,0.0f};
 Player p7={"aaag",0,100,80,10000,10.0f,0.0f};
 Player p8={"aaah",0,100,80,10000,10.0f,0.0f};
 Player p9={"aaai",0,100,80,10000,10.0f,0.0f};
 Player p10={"aaaj",0,100,80,10000,10.0f,0.0f};
 Player p11={"aaak",0,100,80,10000,10.0f,0.0f};
 Player p12={"aaaa",0,100,80,10000,10.0f,0.0f};
 Player p13={"a",0,100,80,10000,10.0f,0.0f};
 Player p14={"aaaaaaaaaaa",0,100,80,10000,10.0f,0.0f}; 
 Player p15={"aaan",0,100,80,10000,10.0f,0.0f};
 Player p16={"aaal",0,100,80,10000,10.0f,0.0f};
 Player p17={"aaam",0,100,80,10000,10.0f,0.0f};

 
 Player empty1;
 Player empty2;
 Player **emptyPeople=(Player**)malloc(sizeof(Player*)*2);
 

 //Yeah, I know... poor form...
 people[0]=&p1;
 people[1]=&p2;
 people[2]=&p3;
 people[3]=&p4;
 people[4]=&p5;
 people[5]=&p6;
 people[6]=&p7;
 people[7]=&p8;
 people[8]=&p9;
 people[9]=&p10;
 people[10]=&p11;
 people[11]=&p12;
 people[12]=&p13;
 people[13]=&p14;
 people[14]=&p15;
 people[15]=&p16;
 people[16]=&p17;
 
 emptyPeople[0]=&empty1;
 emptyPeople[1]=&empty2;
 sdbm_insert("ValidName",emptyPeople[0]);
 sdbm_insert("ValidName2",emptyPeople[1]);
 
 for(int i=0;i<nPlay;i++)
   {
     sdbm_insert(people[i]->name,people[i]);
   }


 //test get
 Player* getTest=(Player*)malloc(sizeof(Player)); 

 sdbm_get("this will not work",getTest);
 printf("error: %d\n",sdbm_error());
 sdbm_get(people[5]->name,getTest);

 printf("getTest stats sample: %s %d %d\n",getTest->name,getTest->playerID,getTest->health);

 free(getTest);

 //test put
 Player* putTest=(Player*)malloc(sizeof(Player));

 sdbm_put("this will not work",putTest);
 printf("error: %d\n",sdbm_error());
 sdbm_put(people[1]->name,people[0]);
 printf("error: %d\n",sdbm_error());
 sdbm_put("aaac",people[16]); //should become "aaam"

 free(putTest);

 //test remove
 sdbm_remove("aaaa");
 printf("error: %d\n",sdbm_error());
 sdbm_remove("aaaa");
 printf("error: %d\n",sdbm_error()); 
 sdbm_remove("aaab");
 printf("error: %d\n",sdbm_error());
 sdbm_remove("aaac");
 printf("error: %d\n",sdbm_error());
 sdbm_remove("aaad");
 sdbm_remove("aaae");
 sdbm_remove("aaaf");
 sdbm_remove("aaag");
 sdbm_remove("aaah");
 sdbm_remove("aaai");
 sdbm_remove("aaaj");
 sdbm_remove("aaak");
 sdbm_remove("aaal");

  sdbm_close();

  sdbm_open(dtb);
  sdbm_open(dtb);

  sdbm_close();
  
  free(people);
  free(emptyPeople); 
  
  return 0;
}
