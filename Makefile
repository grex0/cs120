#Greg Langer/600.120/June 14 2013/Assignment 4/646.265.1591/glanger1/greglanger120@gmail.com 
#Took 30 minutes. Hardest part was realizing the "clean" needed to be tabbed in.
#I'm still not sure if I need to write a reflection for makefiles... anyway, I learned makefiles a lot better than I did when I tried for the last assignment. I was struggling for a long time with the tabs. Now I'm just relieved that I finished this whole assignment by a reasonable time.

CFLAGS = -std=c99 -pedantic -Wall -Wextra -O -g
CC=gcc

game: game.o sdbm.o
	$(CC) $(CFLAGS) -o game game.o sdbm.o

stability: stability.o sdbm.o
	$(CC) $(CFLAGS) -pg -o stability stability.o sdbm.o

coverage: coverage.o sdbm.o
	$(CC) $(CFLAGS)  -fprofile-arcs -ftest-coverage -o coverage coverage.o sdbm.o

game.o: game.c
stability.o: stability.c
coverage.o: coverage.c
sdbm.o: sdbm.c

clean:
	\rm -i *.o *~ a.out stability coverage game

